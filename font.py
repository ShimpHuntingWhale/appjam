# -*- coding: utf-8 -*-
import pygame
from Position import Position

class Font:
    def __init__(self, size, color, bold=False, italic=False):
        self.size, self.color = size, color
        self.sf = pygame.font.SysFont('나눔고딕', size, bold, italic)
        self.lastPos = Position([0, 0])
        
    def render(self, screen, text, pos):
        screen.blit(self.sf.render( text, True, self.color ), pos)