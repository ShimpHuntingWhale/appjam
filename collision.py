
from Position    import Position
from Rect        import Rect
from check_value import check_type

def R2PCollision(rect, pos):
    if check_type(rect, Rect) is not None and check_type(pos, Position) is not None:
        if pos.x >= rect.left and pos.x <= rect.right and pos.y >= rect.top and pos.y <= rect.bottom:
            return True
        else:
            return False
    return None