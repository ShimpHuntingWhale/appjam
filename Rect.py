from check_value import check_list

class Rect:
    def __init__(self, rect):
        self.left, self.top, self.right, self.bottom = 0, 0, 0, 0
        self.setRect(rect)
    
    def setRect(self, rect):
        if check_list(rect) is not None:
            self.left   = rect[0]
            self.top    = rect[1]
            self.right  = rect[2]
            self.bottom = rect[3]