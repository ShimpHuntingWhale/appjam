
from abc import abstractmethod, ABCMeta
from Size   import Size
from Camera import Camera

class DefaultScene:
    __metaclass__ = ABCMeta

    def __init__(self):
        self.winSize = Size([450, 800])
        self.camera = Camera(self.winSize, self.winSize, None)

    @abstractmethod
    def init(self):
        pass

    @abstractmethod
    def event(self, e):
        return None
    
    @abstractmethod
    def update(self):
        return None

    @abstractmethod
    def render(self, screen):
        pass