# pylint: disable=E1101

import pygame

from GameScene  import GameScene
from TitleScene import TitleScene
from enum       import IntEnum

class Director:
    class SCENE_INDEX(IntEnum):
        TITLE, GAME = range(2)

    # pygame initalize
    def __init__(self, width, height):
        pygame.init()
        self.size = (width, height)
        self.screen = pygame.display.set_mode(self.size)
        self.sprite_screen = pygame.display.set_mode(self.size)
        self.clock = pygame.time.Clock()
        pygame.display.set_caption("너가 생각 나는 밤")

        self.isLoop = True
    
        self.sceneIndex = self.SCENE_INDEX.GAME
        self.scenes = [
            TitleScene(), GameScene()
        ]
        self.sc = self.scenes[self.sceneIndex]
    
    # event
    def event(self):
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                self.isLoop = False
            else:
                self.changeScene(self.sc.event(e))
    
    # update
    def update(self):
        self.changeScene(self.sc.update())

    # render
    def render(self):
        # 화면 클리어
        self.screen.fill((255, 255, 255))

        # 씬 그리기
        self.sc.render(self.screen)

        # 화면 전환
        pygame.display.flip()

    # Change Scene
    def changeScene(self, index):
        if index is not None:
            if index >= 0 and index < len(self.scenes):
                self.sceneIndex = index
                self.sc = self.scenes[index]
                self.sc.init()

    # Main Loop
    def loop(self):
        while self.isLoop:
            self.event()
            self.update()
            self.render()

            # 60fps
            self.clock.tick(60)
        else:
            pygame.quit()