# pylint: disable=E1101

import pygame
from Mans import Woman, Man
from Position import Position

class ChatDirector:
    def __init__(self, woman_pictual, woman_chat, man_pictual, man_chat):
        self.y = 100
        self.padding = 100
        self.index = 0
        self.min_height = 100
        self.max_height = 850

        self.chats = []

        self.woman_pictual = woman_pictual
        self.woman_chat = woman_chat
        self.man_pictual = man_pictual
        self.man_chat = man_chat

        self.old_mouse_pos = None
        self.now_mouse_pos = None

    def setPosY(self, y):
        if y <= self.min_height and y + self.padding * (self.index + 1) >= self.max_height:
            self.y = y
            for i in range(len(self.chats)):
                self.chats[i].setPos([self.chats[i].pos.x, self.y + 100 * i])

    def set_pos_y_max_height(self):
        self.setPosY(self.max_height - self.padding * (self.index + 1))
    
    def addChat(self, type, text):
        if type == 0:
            self.chats.append(Woman(self.woman_pictual, self.woman_chat))
            self.chats[self.index].setPos([10, self.y + self.index * 100])
        else:
            self.chats.append(Man(self.man_pictual, self.man_chat))
            self.chats[self.index].setPos([440, self.y + self.index * 100])
        self.chats[self.index].setText(text)
        self.index += 1
        self.set_pos_y_max_height()

    def event(self, e):
        if e.type == pygame.MOUSEBUTTONDOWN:
            self.old_mouse_pos = pygame.mouse.get_pos()
        elif e.type == pygame.MOUSEBUTTONUP:
            self.old_mouse_pos = None
    
    def update(self):
        if self.old_mouse_pos is not None:
            self.now_mouse_pos = pygame.mouse.get_pos()
            tmp_y = self.now_mouse_pos[1] - self.old_mouse_pos[1]
            self.setPosY(self.y + tmp_y)
            self.old_mouse_pos = pygame.mouse.get_pos()

    def render(self, screen, camera):
        for chat in self.chats:
            chat.render(screen, camera)