
import pygame
from Font import Font
from Sprite import Sprite
from Position import Position
from text_lengh import text_lengh

class Woman:
    def __init__(self, pictual, chat):
        self.pictual = Sprite(pictual)
        self.chat = Sprite(chat)
        self.text = ''
        self.pos = [0, 0]

        self.name   = Font(15, pygame.color.Color('#000000'), bold = True)
        self.script = Font(15, pygame.color.Color('#000000'), bold = True)
    
    def setText(self, text):
        self.text = text
    
    def setPos(self, pos):
        self.pos = Position(pos)
        self.pictual.setPos(pos)
        self.chat.setPos([pos[0] + self.pictual.rect.right, pos[1] + 10])
    
    def render(self, screen, camera):
        self.pictual.render(screen, camera)
        self.chat.render(screen, camera)
        self.name.render(screen, '그녀', [self.pos.x + self.pictual.rect.right + 10, self.pos.y - 10])
        self.script.render(screen, self.text, [self.pos.x + self.pictual.rect.right + 20, self.pos.y + 25])

class Man:
    def __init__(self, pictual, chat):
        self.pictual = Sprite(pictual)
        self.chat = Sprite(chat)
        self.text = ''
        self.pos = [0, 0]

        self.name   = Font(15, pygame.color.Color('#000000'), bold = True)
        self.script = Font(15, pygame.color.Color('#000000'), bold = True)

        self.pictual.setAnchorPos([1.0, 0.0])
        self.chat.setAnchorPos([1.0, 0.0])
    
    def setText(self, text):
        self.text = text
    
    def setPos(self, pos):
        self.pos = Position(pos)
        self.pictual.setPos(pos)
        self.chat.setPos([pos[0] - self.pictual.rect.right, pos[1] + 10])
    
    def render(self, screen, camera):
        self.pictual.render(screen, camera)
        self.chat.render(screen, camera)
        self.name.render(screen, '김건희', [self.pos.x - self.pictual.rect.right - 50, self.pos.y - 10])
        self.script.render(screen, self.text, [self.chat.drawPos.x + 10, self.pos.y + 25])