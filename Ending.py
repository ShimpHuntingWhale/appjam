
import pygame

from Sprite import Sprite
from Font import Font

class Ending:
    def __init__(self):
        self.isBadEnding = False
        
        self.badEnding  = Sprite('./img/sprite/ending/BadEnding.png')
        self.goodEnding = Sprite('./img/sprite/ending/GoodEnding.png')

        self.head_font = Font(40, pygame.color.Color('#000000'), bold = True)
    
    def init(self):
        pass
    
    def setType(self, type):
        self.isBadEnding = (type == 0)

    def event(self, e):
        pass
    
    def update(self):
        pass
    
    def render(self, screen, camera):
        if self.isBadEnding:
            self.badEnding.render(screen, camera)
            self.head_font.render(screen, 'Bad Ending', [20, 720])
        else:
            self.goodEnding.render(screen, camera)
            self.head_font.render(screen, 'Happy Ending', [20, 720])