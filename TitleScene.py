# pylint: disable=E1101

import pygame

from DefaultScene import DefaultScene
from Sprite import Sprite
from SpriteSheet import SpriteSheet

class TitleScene(DefaultScene):
    def __init__(self):
        DefaultScene.__init__(self)
        self.init()
    
    def init(self):
        pass

    def event(self, e):        
        return None

    def update(self):
        return None
    
    def render(self, screen):
        pass