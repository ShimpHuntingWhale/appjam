# pylint: disable=E1101

from enum import IntEnum
from time import time

import pygame

from Sprite import Sprite
from SpriteSheet import SpriteSheet
from Button import Button
from Font import Font

from InGameScript import InGameScript
from ChatDirector import ChatDirector

from text_lengh import text_lengh

class InGame:
    class BG_INDEX(IntEnum):
        ARROW, BG_TOP, BG_UI, BUTTON, MAN, MAN_CHAT, SEND, WOMAN, WOMAN_CHAT = range(9)

    def __init__(self):        
        self.bg = Sprite('./img/sprite/chat/bg.png')
        self.bg2 = Sprite('./img/sprite/chat/bg2.png')
        self.bg2.setPosY(530)

        self.chat_imgs = SpriteSheet('./img/sprite/chat/chat.png', './img/sprite/chat/chat.json')
        self.chat_imgs[self.BG_INDEX.BG_UI].setAnchorPos([0, 0.5])
        self.chat_imgs[self.BG_INDEX.SEND].setAnchorPos([1.0, 0.5])
        self.chat_imgs[self.BG_INDEX.BG_UI].setPos([-6, 773])
        self.chat_imgs[self.BG_INDEX.SEND].setPos([440, 770])

        self.return_button = Button(self.chat_imgs[self.BG_INDEX.ARROW].img)
        self.return_button.setAnchorPos([0, 0])
        self.return_button.setPos([5, 20])
        
        self.buttons = []
        self.is_show_buttons = False

        self.title_font = Font(20, pygame.color.Color('#ffffff'), bold=True)

        self.chat_director = ChatDirector(
            self.chat_imgs[self.BG_INDEX.WOMAN].img, self.chat_imgs[self.BG_INDEX.WOMAN_CHAT].img,
            self.chat_imgs[self.BG_INDEX.MAN].img, self.chat_imgs[self.BG_INDEX.MAN_CHAT].img
        )

        self.script = InGameScript('./script/Scene_1.json')

        self.old_time = None
        self.sleep_time = None

    def init(self):
        pass
    
    def add_chat(self):
        result = self.script.getLineText()
        if result['type'] == 'man':
            self.chat_director.addChat(1, result['script'])
        elif result['type'] == 'woman':
            self.chat_director.addChat(0, result['script'])
        elif result['type'] == 'sleep':
            self.old_time = time()
            self.sleep_time = float(result['time'])
        elif result['type'] == 'select':
            self.init_button(int(result['lengh']), result['selects'])
        self.script.nextLine()
    
    def init_button(self, count, selects):
        if self.is_show_buttons == False:
            if count >= 1 and count <= 3:
                self.buttons = [ Button(self.chat_imgs[self.BG_INDEX.BUTTON].img) for i in range(count) ]
                center_y = 669
                if count == 1:
                    self.buttons[0].setPosY(center_y)
                    self.buttons[0].set_text(selects['select1']['info'])
                    self.buttons[0].set_next(selects['select1']['nextFile'])
                elif count == 2:
                    for i in range(2):
                        self.buttons[i].setPosY(center_y - 50 + 100 * i)
                        self.buttons[i].set_text(selects['select{}'.format(i + 1)]['info'])
                        self.buttons[i].set_next(selects['select{}'.format(i + 1)]['nextFile'])
                elif count == 3:
                    for i in range(3):
                        self.buttons[i].setPosY(center_y - 80 + 80 * i)
                        self.buttons[i].set_text(selects['select{}'.format(i + 1)]['info'])
                        self.buttons[i].set_next(selects['select{}'.format(i + 1)]['nextFile'])
                self.is_show_buttons = True

                self.chat_imgs[self.BG_INDEX.BG_UI].setPos([-6, 503])
                self.chat_imgs[self.BG_INDEX.SEND].setPos([440, 500])
                self.chat_director.max_height = 600
                self.chat_director.set_pos_y_max_height()
                
    def delete_button(self):
        self.is_show_buttons = False
        self.chat_imgs[self.BG_INDEX.BG_UI].setPos([-6, 773])
        self.chat_imgs[self.BG_INDEX.SEND].setPos([440, 770])
        self.buttons.clear()
        self.chat_director.max_height = 850
        self.chat_director.set_pos_y_max_height()
        
    def event(self, e):
        if self.is_show_buttons:
            for b in self.buttons:
                if b.event(e):
                    self.chat_director.addChat(1, b.text)
                    self.delete_button()
                    if b.next == 'BadEnding':
                        return [2, 0]
                    elif b.next == 'GoodEnding':
                        return [2, 1]
                    else:
                        self.script = InGameScript('./script/{}.json'.format(b.next))
                    break
            self.return_button.event(e)
        self.chat_director.event(e)
        return None
    
    def update(self):
        if self.old_time is not None:
            new_time = time()
            if new_time - self.old_time >= self.sleep_time:
                self.old_time = None
        else:
            self.add_chat()
        self.chat_director.update()
    
    def render(self, screen, camera):
        self.bg.render(screen, camera)
        
        self.chat_director.render(screen, camera)

        if self.is_show_buttons:
            self.bg2.render(screen, camera)
            for b in self.buttons:
                b.render(screen, camera)
        self.chat_imgs[self.BG_INDEX.BG_UI].render(screen, camera)
        self.chat_imgs[self.BG_INDEX.SEND].render(screen, camera)

        self.chat_imgs[self.BG_INDEX.BG_TOP].render(screen, camera)
        self.return_button.render(screen, camera)
        self.title_font.render(screen, '그녀', [225 - text_lengh('그녀') * self.title_font.size / 2, 20])