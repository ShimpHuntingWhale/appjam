
from Position import Position
from Rect     import Rect
from Size     import Size
from check_value import check_list, check_type

class Camera:
    def __init__(self, size, max_size, target):
        self.size     = check_type(size, Size)
        self.max_size = check_type(max_size, Size)

        self.pos = Position([0, 0])

        self.target = 0
        self.setTarget(target)
       
    def setTarget(self, target):
        self.target = target
    
    def update(self):
        tmp_rect = self.target.boundingBox
        tmp_centerPos = Position([
            tmp_rect.left + (tmp_rect.right  - tmp_rect.left) / 2,
            tmp_rect.top  + (tmp_rect.bottom - tmp_rect.top)  / 2
        ])

        if tmp_centerPos.x <= self.pos.x + self.size.width / 2:
            self.pos.x = tmp_centerPos.x - self.size.width / 2
            if self.pos.x <= 0:
                self.pos.x = 0
        else:
            self.pos.x = tmp_centerPos.x - self.size.width / 2
            if self.pos.x >= self.max_size.width - self.size.width:
                self.pos.x = self.max_size.width - self.size.width
        
        if tmp_centerPos.y <= self.pos.y + self.size.height / 2:
            self.pos.y = tmp_centerPos.y - self.size.height / 2
            if self.pos.y <= 0:
                self.pos.y = 0
        else:
            self.pos.y = tmp_centerPos.y - self.size.height / 2
            if self.pos.y >= self.max_size.height - self.size.height:
                self.pos.y = self.max_size.height - self.size.height
    
    def getPos(self):
        return self.pos.getPosition()
    
    def getPosX(self):
        return self.pos.x
    
    def getPosY(self):
        return self.pos.y

    def isInCamera(self, sprite):
        if id(sprite) == id(self.target):
            return True

        r1 = sprite.boundingBox
        r2 = Rect([self.pos.x, self.pos.y, self.pos.x + self.size.width, self.pos.y + self.size.height])

        if r1.left <= r2.right and r1.right >= r2.left and r1.top <= r2.bottom and r1.bottom >= r2.top:
            return True
        else:
            return False