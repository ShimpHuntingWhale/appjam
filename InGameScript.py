
import json
from pygame import color
from Sprite import Sprite
from Font import Font

class InGameScript:
    def __init__(self, filepath):
        with open(filepath, 'r', encoding='UTF8') as f:
            self.scripts = json.load(f)
        self.index = 0
        self.select_script = None
    
    def getLineText(self):
        return self.scripts[self.index]
    
    def nextLine(self):
        if self.index < len(self.scripts) - 1:
            self.index += 1