
import json
from pygame import color
from Sprite import Sprite
from Font import Font
from DialogFont import DialogFont

class Script:
    def __init__(self, filepath):
        with open(filepath, 'r', encoding='UTF8') as f:
            self.scripts = json.load(f)
        self.index = 0

        self.dialogue_window = Sprite('./img/sprite/dialogue.png')
        self.dialogue_window.setPos([225, 790])
        self.dialogue_window.setAnchorPos([0.5, 1.0])

        self.cursur = Sprite('./img/sprite/cursur.png')

        self.name_font     = Font(20, color.Color('#000000'), bold = True)
        self.contents_font = DialogFont(20, color.Color('#000000'))

        self.name     = ''
        self.contents = ''
        self.getLineText()

        self.select_script = None
    
    def getLineText(self):
        tmp_scripts = self.scripts[self.index]
        type = list(tmp_scripts.keys())[0]
        if tmp_scripts[type] == 'script':
            self.name = list(tmp_scripts.keys())[1]
            self.contents = tmp_scripts[self.name]
            self.contents_font.init(self.contents)
        
        elif tmp_scripts[type] == 'narration':
            self.name = ''
            self.contents = tmp_scripts[list(tmp_scripts.keys())[1]]
            self.contents_font.init(self.contents)

        # sleep, scene, select
        else:
            if tmp_scripts[type] == 'select':
                self.select_script = tmp_scripts
            return tmp_scripts[type]
        
        return None
    
    def nextLine(self):
        if self.contents_font.is_show_cursur:
            self.index += 1
            return self.getLineText()
    
    def update(self):
        self.contents_font.update()
    
    def render(self, screen, camera):
        self.dialogue_window.render(screen, camera)
        self.name_font.render(screen, self.name, [45, 470])
        self.contents_font.render(screen)

        if self.contents_font.is_show_cursur:
            self.cursur.setPos(self.contents_font.lastPos.getPosition())
            self.cursur.render(screen, camera)