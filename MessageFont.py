
from Font import Font

class MessageFont(Font):
    def __init__(self, fontname, size, color, bold=False, italic=False):
        Font.__init__(self, fontname, size, color, bold, italic)
        
    def render(self, screen, text, pos):
        screen.blit(self.sf.render( text, True, self.color ), pos)