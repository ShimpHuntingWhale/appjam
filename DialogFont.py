
from Font import Font

class DialogFont(Font):
    def __init__(self, size, color, bold=False, italic=False):
        Font.__init__(self, size, color, bold, italic)
        self.MAXSIZE = 19

        self.pos = [40, 520]

        self.index = 0
        self.text = ''
        self.texts = ['' for i in range(8)]
        self.lengh = 0
        self.height_len = 0
        self.min_index = 0

        self.loop = True
        self.is_show_cursur = False
    
    def init(self, text):
        self.index = 0
        self.text = text
        self.texts.clear()
        self.texts = ['' for i in range(8)]
        self.lengh = 0
        self.height_len = 0
        self.min_index = 0

        self.loop = True
        self.is_show_cursur = False

    def update(self):
        if self.loop:
            self.index += 1
            if self.index >= len(self.text) - 1:
                self.index = -1
                self.loop = False
                self.is_show_cursur = True

            if self.text[self.index] == ' ' or self.text[self.index] == '.':
                self.lengh += 0.3
            else:
                self.lengh += 1.0
            
            if self.lengh >= self.MAXSIZE:
                self.height_len += 1
                self.lengh = 0
                self.min_index = self.index
            
            self.lastPos.setPosition([self.pos[0] + (self.lengh + 1) * self.size, self.pos[1] + (self.size + 10) * self.height_len + 2.5])

            if self.loop:
                self.texts[self.height_len] = self.text[self.min_index:self.index+1]
            else:
                self.texts[self.height_len] = self.text[self.min_index:]
        
    def render(self, screen):
        i = 0
        for s in self.texts:
            if s is not '':
                screen.blit(self.sf.render(s, True, self.color), [self.pos[0], self.pos[1] + (self.size + 10) * i])
                i += 1