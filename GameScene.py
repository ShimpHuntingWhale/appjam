# pylint: disable=E1101

import pygame
from enum import IntEnum

from DefaultScene import DefaultScene

from Opening import Opening
from InGame  import InGame
from Ending  import Ending
from Script  import Script

class GameScene(DefaultScene):
    class LAYER_INDEX(IntEnum):
        OPENING, INGAME, ENDING = range(3)

    def __init__(self):
        # initalize once
        DefaultScene.__init__(self)

        self.layers = [ Opening(), InGame(), Ending() ]
        self.layer_index = 0

        self.init()

    # initalize load scene
    def init(self):
        for i in self.layers: i.init()
        self.layer_index = self.LAYER_INDEX.OPENING
        
    def changeLayer(self, index):
        if index == -1:
            pygame.quit()
            pass

        if index is not None:
            if type(index) == list:
                self.layer_index = self.LAYER_INDEX.ENDING
                self.layers[self.layer_index].init()
                self.layers[self.layer_index].setType(index[1])
            else:
                self.layer_index = index
                self.layers[self.layer_index].init()

    def event(self, e):
        self.changeLayer(self.layers[self.layer_index].event(e))
        return None
    
    def update(self):
        self.layers[self.layer_index].update()
        return None
        
    def render(self, screen):
        self.layers[self.layer_index].render(screen, self.camera)