
import pygame

class Audio:
    def __init__(self, filepath, mode):
        self.filepath = filepath
        self.sound = pygame.mixer.Sound(filepath)
        self.mode = mode

        self.loops = 0
        self.set_mode(mode)
    
    def play(self, maxtime=0, fade_ms=0):
        self.sound.play(self.loops, maxtime, fade_ms)

    def stop(self):
        self.sound.stop()

    def set_volumne(self, value):
        value = value if value >= 0.0 else 0.0
        value = value if value <= 1.0 else 1.0
        self.sound.set_volumne(value)
    
    def set_mode(self, mode):
        if mode == "loop":
            self.loops = -1
        elif mode == "once":
            self.loops = 0