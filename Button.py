# pylint: disable=E1101

import pygame
from Position import Position
from Sprite import Sprite
from Font import Font
from collision import R2PCollision
from text_lengh import text_lengh

class Button(Sprite):
    def __init__(self, file):
        Sprite.__init__(self, file)
        self.setPosX(225)
        self.setAnchorPos([0.5, 0.5])
        self.text = ''
        self.next = ''
        self.font = Font(20, pygame.color.Color('#000000'), bold=True)
    
    def set_text(self, text):
        self.text = text
    
    def set_next(self, next):
        self.next = next
    
    def is_in_mouse(self, e):
        if R2PCollision(self.boundingBox, Position(list(pygame.mouse.get_pos()))):
            return True
        return False

    def event(self, e):
        if e.type == pygame.MOUSEBUTTONDOWN:
            if self.is_in_mouse(e):
                return True
        return False
    
    def render(self, screen, camera):
        Sprite.render(self, screen, camera)
        self.font.render(screen, self.text, [self.pos.x - text_lengh(self.text) / 2 * self.font.size, self.pos.y - self.font.size / 2 - 2])