# pylint: disable=E1101

import pygame
from Animation import Animation
from AnimationDirector import AnimationDirector
from Script import Script
from Sprite import Sprite
from Button import Button

class Opening:
    def __init__(self):
        self.bg_animation = AnimationDirector([Animation(('./img/sprite/InGame/bg.png', './img/sprite/InGame/bg2.png'), 500)])
        self.bg = Sprite('./img/sprite/InGame/bg.png')

        self.dialog = Script('./script/Scene_0.json')
        self.dialog_state = 0

        self.buttons = []
        self.is_show_button = False
    
    def init(self):
        self.buttons.clear()
        self.dialog_state = 0

    def event(self, e):
        if self.is_show_button:
            for b in self.buttons:
                if b.event(e):
                    if b == self.buttons[0]:
                        return 1
                    else:
                        return [2, 0]
        else:
            if e.type == pygame.MOUSEBUTTONDOWN:
                self.dialog_state = self.dialog.nextLine()
        return None
        
    def init_button(self, script):
        self.buttons.clear()

        button_lengh = script['lengh']
        for i in range(button_lengh):
            self.buttons.append( Button('./img/sprite/button.png') )
            if button_lengh % 2 == 0:
                self.buttons[i].setPosY(400 - (button_lengh / 2 * self.buttons[i].rect.bottom) / 2 + i * (self.buttons[i].rect.bottom + 40))
            else:
                self.buttons[i].setPosY(400 - button_lengh / 2 * self.buttons[i].rect.bottom + i * (self.buttons[i].rect.bottom + 40))
            self.buttons[i].set_text(script['selects']['select{}'.format(i + 1)]['info'])
    
    def update(self):
        self.bg_animation.update(self.bg)
        self.dialog.update()

        if self.dialog_state is not None:
            if self.dialog_state == 'select':
                self.is_show_button = True
                self.init_button(self.dialog.select_script)
    
    def render(self, screen, camera):
        self.bg.render(screen, camera)
        self.dialog.render(screen, camera)
        if self.is_show_button:
            for b in self.buttons:
                b.render(screen, camera)