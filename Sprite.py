# pylint: disable=E1121

import pygame
from Image       import Image
from Position    import Position
from Rect        import Rect
from check_value import check_list, check_type

class Sprite(Image):
    def __init__(self, filepath):
        Image.__init__(self, filepath)

        self.pos         = None
        self.anchorP     = None
        self.drawPos     = None
        self.boundingBox = None
        self.init()
    
    def init(self):
        Image.init(self)
        
        self.pos         = Position([0, 0])
        self.anchorP     = Position([0, 0])
        self.drawPos     = Position([0, 0])
        self.boundingBox = Rect([0, 0, 0, 0])
        self.setBoundigBox()

    # Position 관련 함수들
    def setPos(self, pos):
        self.pos.setPosition(pos)
        self.setBoundigBox()

    def setPosX(self, x):
        self.pos.setPositionX(x)
        self.setBoundigBox()
        
    def setPosY(self, y):
        self.pos.setPositionY(y)
        self.setBoundigBox()

    def getPos(self):
        return self.pos.getPosition()

    # AnchorPosition 관련 함수들
    def setAnchorPos(self, anchorPos):
        self.anchorP.setPosition(anchorPos)
        self.setDrawPosition()

    def setDrawPosition(self, camera_pos=Position([0, 0])):
        self.drawPos.setPosition([
            self.pos.x - self.rect.right  * self.anchorP.x - camera_pos.x,
            self.pos.y - self.rect.bottom * self.anchorP.y - camera_pos.y
        ])
    
    def setBoundigBox(self):
        self.boundingBox.setRect([
            self.pos.x - self.rect.right  * self.anchorP.x,
            self.pos.y - self.rect.bottom * self.anchorP.y,
            self.pos.x + self.rect.right  * (1 - self.anchorP.x),
            self.pos.y + self.rect.bottom * (1 - self.anchorP.y)
        ])

    def changeImg(self, img):
        self.img = check_type(img, Image).img
        if self.img is None:
            self.img = pygame.image.load('./img/sprite/none.png')

    def render(self, screen, camera):
        if camera.isInCamera(self):
            self.setDrawPosition(camera.pos)

            if self.scale[0] != 1 and self.scale[1] != 1:
                tmp = pygame.Surface( (self.img.get_width(), self.img.get_height()) ).convert()
                tmp.blit(screen, (-self.drawPos.x, -self.drawPos.y))
                tmp.blit(self.img, (0, 0))
                tmp.set_alpha(self.opacity * 255)
                screen.blit(tmp, self.drawPos)
            else:
                screen.blit(self.img, self.drawPos.getPositionByTuple())